import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from './entities';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  
  constructor(private http:HttpClient) { }

  getAll():Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/api/article');
  }

  getById(id:number):Observable<Article> {
    return this.http.get<Article>('http://localhost:8080/api/article/'+id);
  }
  add(article:Article){
    return this.http.post<Article>('http://localhost:8080/api/article/', article);
  }
  delete(id:number) {
    return this.http.delete('http://localhost:8080/api/article/'+id);
  }
  put(article:Article){
    return this.http.put<Article>('http://localhost:8080/api/article/'+ article.id, article);
  }

}





