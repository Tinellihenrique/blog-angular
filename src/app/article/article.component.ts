import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  selected?:Article;
  article: Article[] = [];
  displayEditForm:boolean = false;
  modifiedAr:Article = {
    text: "", title: "", image: ""
  };


  constructor(private artService: ArticleService) { }

  ngOnInit(): void {
    this.artService.getAll().subscribe(data => this.article = data);
  }

  fetchPerson(id:number) {
    this.artService.getById(id).subscribe(data => this.selected = data);
  }  

  delete(id:number) {
    this.artService.delete(id).subscribe();
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.article.forEach((operation, operation_index) => {
      if(operation.id == id) {
        this.article.splice(operation_index, 1);
      }
    });
  }

  showEditForm(article:Article) {
    this.displayEditForm = true;
    this.modifiedAr = Object.assign({},article);
  }

  update(article:Article) {
    this.artService.put(article).subscribe();
    this.updateOperationInList(article);
    this.displayEditForm = false;
  }

  updateOperationInList(article:Article) {
    this.article.forEach((op_list) => {
      if(op_list.id == article.id) {
        op_list.title = article.title;
        op_list.text = article.text;
        op_list.date = article.date;
        op_list.image = article.image;
        
      }
    });
  }

}

