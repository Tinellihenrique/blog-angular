import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleSoloComponent } from './article-solo.component';

describe('ArticleSoloComponent', () => {
  let component: ArticleSoloComponent;
  let fixture: ComponentFixture<ArticleSoloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleSoloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleSoloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
