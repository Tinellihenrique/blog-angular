import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-article-solo',
  templateUrl: './article-solo.component.html',
  styleUrls: ['./article-solo.component.css']

})
export class ArticleSoloComponent implements OnInit {
  routeId: any;
  item?: Article;
  selected?:Article;
  article: Article[] = [];
  displayEditForm:boolean = false;
  modifiedAr:Article = {
    text: "", title: "", image: ""
  };


  constructor(private route:ActivatedRoute, private service:ArticleService) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    
    this.service.getById(Number(this.routeId)).subscribe(data => this.item = data);
    
    // showEditForm(article:Article) {
    //   this.displayEditForm = true;
    //   this.modifiedAr = Object.assign({},article);
    // }
  
  
  }
}
