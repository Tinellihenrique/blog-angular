import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
  
})
export class RegisterComponent implements OnInit {

  article:Article = {
      id:0,
      text:"",
      title:"",
      image : "",
      date: "",
 
  };

  constructor(private artService:ArticleService, private router:Router) { }

  ngOnInit(): void {
  }
  addOperation() {
    this.artService.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    }); 

  }
}