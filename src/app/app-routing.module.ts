import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleSoloComponent } from './article-solo/article-solo.component';
import { ArticleComponent } from './article/article.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:"",component:ArticleComponent}, 
  {path:"register",component:RegisterComponent},//http://localhost:4200/register
  {path:"article/:id",component:ArticleSoloComponent}
// nao sei o que é mais tem que fazer para cada pagina 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
