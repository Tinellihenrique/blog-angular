

export interface Article {
    id?:number;
    text:string;
    title:string;
    image : string;
    date?: string;
    
}

export interface Comments {
    id:number;
    text: string;
    date: string

}
// public class ArticleEntity {
//     private Integer id;
//     private String text;
//     private String title;
//     private String image;
//     private LocalDate date;

// public class CommentsEntity {
//     private Integer id;
//     private String text;
//     private LocalDate date;